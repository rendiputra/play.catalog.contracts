# Guide

### At project Play.Catalog.Service write this command:

```bash
dotnet add reference <to path>/Play.Catalog.Contracts.csproj
```

### Example:
```bash
dotnet add reference ../../../Play.Catalog.Contracts/Play.Catalog.Contracts/Play.Catalog.Contracts.csproj
```